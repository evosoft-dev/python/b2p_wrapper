from setuptools import setup, find_packages
from src.wrapper_b2p import __version__


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='wrapper_b2p',
    version=__version__,
    author='Aleksey Tarasov',
    author_email='ataraspost@gmail.com',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    url='',
    license='LICENSE.rst',
    description='wrapper to best2pay',
    long_description_content_type='text/markdown',
    long_description=long_description,
    install_requires=[
        "requests >= 2.26.0",
        "xmltodict >= 0.12.0",
    ],
)
