import logging
import os

import enquiries
from dotenv import load_dotenv

from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p import (Register, Purchase, Authorize, Complete, Reverse, Operation, Order,
                             PurchaseByToken, AuthorizeByToken)

load_dotenv()

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

SECTOR = os.environ.get('B2P_SECTOR')

logging.info(SECTOR)

TOKEN = os.environ.get('B2P_TOKEN')

logging.info(TOKEN)

TOKEN_FOR_PAY = os.environ.get('TOKEN_FOR_PAY')

AMOUNT = 1000
DESCRIPTION = 'test'

ORDER_ID = None
OPERATION_ID = None


def create_order():
    order_ = Register(SECTOR, 10000, 'description', TOKEN)
    order_dict = order_.do()
    logging.info(order_dict)
    global ORDER_ID
    ORDER_ID = order_dict['order']['id']
    return ORDER_ID


def processing_purchase():
    order_id = create_order()
    purchase = Purchase(SECTOR, order_id, TOKEN)
    res = purchase.do()
    logging.info('URL для оплаты')
    logging.info(res)
    input('После завершения платежа по ссылке нажмите ENTER')


def processing_authorize():
    order_id = create_order()
    purchase = Authorize(SECTOR, order_id, TOKEN)
    res = purchase.do()
    logging.info('URL для холдирования')
    logging.info(res)
    input('После завершения платежа по ссылке нажмите ENTER')


def complete():
    processing_authorize()
    complete_ = Complete(SECTOR, ORDER_ID, 10000, TOKEN)
    res = complete_.do()
    logging.info(res)


def reverse():
    processing_purchase()
    reverse_ = Reverse(SECTOR, ORDER_ID, 1000, TOKEN)
    res = reverse_.do()
    global OPERATION_ID
    OPERATION_ID = res['operation']['id']
    logging.info(res)


def operation():
    reverse()
    operation_ = Operation(SECTOR, ORDER_ID, OPERATION_ID, TOKEN)
    res = operation_.do()
    logging.info(res)


def order():
    processing_purchase()
    order_ = Order(SECTOR, TOKEN, ORDER_ID, get_token=1)
    res = order_.do()
    logging.info(res)


def authorize_by_token():
    create_order()
    authorize_by_token_ = AuthorizeByToken(SECTOR, ORDER_ID, TOKEN_FOR_PAY, TOKEN)
    res = authorize_by_token_.do()
    logging.info(res)


def purchase_by_token():
    create_order()
    purchase_by_token_ = PurchaseByToken(SECTOR, ORDER_ID, TOKEN_FOR_PAY, TOKEN)
    res = purchase_by_token_.do()
    logging.info(res)


def factory_response_to_b2p(type_: str):
    if type_ == 'Create order':
        create_order()
    if type_ == 'Purchase':
        processing_purchase()
    if type_ == 'Authorize':
        processing_authorize()
    if type_ == 'Complete':
        complete()
    if type_ == 'Reverse':
        reverse()
    if type_ == 'Operation':
        operation()
    if type_ == 'Order':
        order()
    if type_ == 'Authorize by token':
        authorize_by_token()
    if type_ == 'Purchase by token':
        purchase_by_token()


def run_script():
    while True:
        options = ['Create order', 'Purchase', 'Authorize', 'Complete', 'Reverse', 'Operation', 'Order',
                   'Authorize by token', 'Purchase by token', 'Quite', ]
        response = enquiries.choose('Pick a thing', options)
        if response == 'Quite':
            break
        try:
            factory_response_to_b2p(response)
        except ErrorConnectB2P as err:
            print(str(err))


if __name__ == '__main__':
    run_script()
