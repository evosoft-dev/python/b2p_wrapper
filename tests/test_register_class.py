from collections import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import Register
from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE = 'ZmUzY2E0ODU3MTM5MjRiZTA3YmFhNmU4YmRmMzYxZjk='


def test_init_register(register):
    assert register.fee == 300
    assert register.lang == 'RU'


def test_init_with_not_allowed_field():
    register = Register('0000', 1000, 'test', 'test', fee=300, not_allowed_field='test')

    assert not hasattr(register, 'not_allowed_field')


def test_create_signature(register):
    assert register._create_signature() == SIGNATURE  # pylint: disable=protected-access


def test_get_url_b2p_test_stand(register):
    assert register._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_prod_stand():
    register = Register('0000', 1000, 'test', 'test', fee=300, is_prod_stand=True)
    assert register.is_prod_stand
    assert register._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_custom_stand():
    register = Register('0000', 1000, 'test',
                        'test', fee=300, stand_url='https://custom.best2pay.net')

    assert register._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_get_date_by_send_b2p(register):
    assert register._get_date_by_send_b2p() == {'amount': 1000,  # pylint: disable=protected-access
                                                'currency': 643,
                                                'description': 'test',
                                                'failurl': '',
                                                'fee': 300,
                                                'lang': 'RU',
                                                'sector': '0000',
                                                'signature': SIGNATURE,
                                                'url': ''
                                                }

    assert 'is_prod_stand' in register.__dict__
    assert 'b2p_token' in register.__dict__


def test_do_response_success_to_b2p(mocker, response_xml_success_b2p, register):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())

    order = register.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Register',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'description': 'test',
            'failurl': '',
            'fee': 300,
            'lang': 'RU',
            'sector': '0000',
            'signature': SIGNATURE,
            'url': ''
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_error_to_b2p(mocker, response_xml_error_b2p, register):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = register.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Register',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'description': 'test',
            'failurl': '',
            'fee': 300,
            'lang': 'RU',
            'sector': '0000',
            'signature': SIGNATURE,
            'url': ''
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, register):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        register.do()

    assert mock.call_count == 3
