import pytest

from src.wrapper_b2p import Purchase
from src.wrapper_b2p.exception import ErrorPayerIDNotExist
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE_WITHOUT_SAVE_CARD = 'MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='
SIGNATURE_WITH_SAVE_CARD = 'M2ZmNzk0ZWY2N2Y5ZjJiYjRjZjQ5ZTJjNTM4NmQyODI='


def test_default_init_purchase():
    purchase = Purchase('0000', '1', 'test')

    assert purchase.sector == '0000'
    assert purchase.id == '1'
    assert purchase.b2p_token == 'test'
    assert purchase.get_token == 0
    assert not purchase.token
    assert not purchase.payer_id
    assert not purchase.save_card
    assert not purchase.is_prod_stand


def test_init_purchase():
    purchase = Purchase('0000', '1', 'test', **{
        'get_token': 1,
        'token': 'token',
        'payer_id': '1',
        'save_card': 'true',
        'is_prod_stand': True
    })
    assert purchase.sector == '0000'
    assert purchase.id == '1'
    assert purchase.b2p_token == 'test'
    assert purchase.get_token == 1
    assert purchase.token == 'token'
    assert purchase.payer_id == '1'
    assert purchase.save_card == 'true'
    assert purchase.is_prod_stand


def test_init_not_allowed_params():
    purchase = Purchase('0000', '1', 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Purchase' object has no attribute 'not_valid_field'"):
        purchase.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(purchase):
    assert purchase._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    purchase = Purchase('0000', '1', 'test', is_prod_stand=True)

    assert purchase._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    purchase = Purchase('0000', '1', 'test', stand_url='https://custom.best2pay.net')

    assert purchase._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature_without_save_card(purchase):
    assert purchase._create_signature() == SIGNATURE_WITHOUT_SAVE_CARD  # pylint: disable=protected-access


def test_create_signature_with_save_card():
    purchase = Purchase('0000', '1', 'test', save_card='true', payer_id=1)

    assert purchase._create_signature() == SIGNATURE_WITH_SAVE_CARD  # pylint: disable=protected-access


def test_create_signature_raise_exception():
    with pytest.raises(ErrorPayerIDNotExist, match=''):
        Purchase('0000', '1', 'test', save_card='true')


@pytest.mark.parametrize(
    'purchase_instance, get_params',
    [
        (Purchase('0000', '1', 'test'), 'sector=0000&id=1&'
                                        'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='),
        (Purchase('0000', '1', 'test', get_token=1), 'sector=0000&id=1&get_token=1&'
                                                     'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='),
        (Purchase('0000', '1', 'test', get_token=1, payer_id=1), 'sector=0000&id=1&get_token=1&p'
                                                                 'ayer_id=1&signature=MWU0N2MyODBiOTUyZWYyYzljZ'
                                                                 'TI4N2JkODUzYWRlNGI='),
    ]
)
def test_get_params(purchase_instance, get_params):
    assert purchase_instance._get_params() == get_params  # pylint: disable=protected-access


def test_do(purchase):
    assert purchase.do() == 'https://test.best2pay.net/webapi/Purchase?sector=0000&id=1&' \
                            'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='
