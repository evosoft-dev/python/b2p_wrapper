import pytest

from src.wrapper_b2p import Authorize


@pytest.fixture()
def authorize():
    return Authorize('0000', '1', 'test')
