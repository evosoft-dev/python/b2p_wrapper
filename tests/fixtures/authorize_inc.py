import pytest

from src.wrapper_b2p import AuthorizeInc


@pytest.fixture()
def authorize_inc():
    return AuthorizeInc('0000', '1', 1000, 'test')
