import pytest

from src.wrapper_b2p import Operation


@pytest.fixture()
def operation():
    return Operation('0000', '1', '1', 'test')
