import pytest

from src.wrapper_b2p import AuthorizeByToken


@pytest.fixture()
def authorize_by_token():
    return AuthorizeByToken('0000', '1', 'token', 'test')
