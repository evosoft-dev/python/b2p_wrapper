import pytest

from src.wrapper_b2p import Order


@pytest.fixture()
def order():
    return Order('0000', 'test', id_='1')
