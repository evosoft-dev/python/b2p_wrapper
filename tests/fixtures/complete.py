import pytest

from src.wrapper_b2p import Complete


@pytest.fixture()
def complete():
    return Complete('0000', '1', 1000, 'test')
