import pytest

from src.wrapper_b2p import PurchaseByToken


@pytest.fixture()
def purchase_by_token():
    return PurchaseByToken('0000', '1', 'token', 'test')
