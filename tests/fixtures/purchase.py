import pytest

from src.wrapper_b2p import Purchase


@pytest.fixture()
def purchase():
    return Purchase('0000', '1', 'test')
