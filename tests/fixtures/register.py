import pytest

from src.wrapper_b2p import Register

XML_SUCCESS = '''<?xml version="1.0" encoding="UTF-8"?>
            <order>
                <id>1297336</id>
                <state>REGISTERED</state>
                <inprogress>0</inprogress>
                <date>2021.10.06 05:58:04</date>
                <amount>1000</amount>
                <currency>643</currency>
                <reference>7a8f69d2-0ebb-4f58-9e67-3a62ff1359cb</reference>
                <description>test</description>
                <url/>
                <parameters number="6">
                    <parameter>
                        <name>amount_actual</name>
                        <value>1000</value>
                    </parameter>
                    <parameter>
                        <name>b2p_token</name>
                        <value>test</value>
                    </parameter>
                    <parameter>
                        <name>fee</name>
                        <value>300</value>
                    </parameter>
                    <parameter>
                        <name>lang</name>
                        <value>RUSSIAN</value>
                    </parameter>
                    <parameter>
                        <name>paypage_open_count</name>
                        <value>0</value>
                    </parameter>
                    <parameter>
                        <name>payments_executed</name>
                        <value>0</value>
                    </parameter>
                </parameters>
                <signature>NjlkNTRkNDg3MTZhZTNhNmUwYzgzZjNlYzMxMTY3YTM=</signature>
            </order>'''

XML_ERROR = '''<?xml version="1.0" encoding="UTF-8"?>
                <error>
                    <description>Ошибка: неверная цифровая подпись.Пожалуйста, обратитесь в свой Интернет-магазин.
                    </description>
                    <code>109</code>
                </error>'''


@pytest.fixture()
def register():
    return Register('0000', 1000, 'test', 'test', fee=300)


@pytest.fixture()
def response_xml_error_b2p():
    return XML_ERROR


@pytest.fixture()
def response_xml_success_b2p():
    return XML_SUCCESS
