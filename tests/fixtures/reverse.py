import pytest

from src.wrapper_b2p import Reverse


@pytest.fixture()
def reverse():
    return Reverse('0000', '1', 1000, 'test')
