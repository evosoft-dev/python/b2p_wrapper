from wrapper_b2p import settings
from wrapper_b2p.wrapper_class.mixin import MixinGetURLStandB2P


class ExampleClass(MixinGetURLStandB2P):
    def __init__(self, stand_url: str = None, is_prod_stand: bool = False):
        self.stand_url = stand_url
        self.is_prod_stand = is_prod_stand


def test_get_url_b2p_prod_stand():
    example = ExampleClass(is_prod_stand=True)

    assert example._get_url_b2p() == settings.URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_test_stand():
    example = ExampleClass()
    assert example._get_url_b2p() == settings.URL_TEST_STAND  # pylint: disable=protected-access

    example = ExampleClass(is_prod_stand=False)
    assert example._get_url_b2p() == settings.URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_stand_url():
    example = ExampleClass(stand_url='https://example.com')
    assert example._get_url_b2p() == 'https://example.com'  # pylint: disable=protected-access


def test_get_url_b2p_stand_url_and_prod_stand():
    example = ExampleClass(stand_url='https://example.com', is_prod_stand=True)
    assert example._get_url_b2p() == 'https://example.com'  # pylint: disable=protected-access


def test_get_url_b2p_stand_url_and_test_stand():
    example = ExampleClass(stand_url='https://example.com', is_prod_stand=False)
    assert example._get_url_b2p() == 'https://example.com'  # pylint: disable=protected-access
