from typing import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import Order
from src.wrapper_b2p.exception import ErrorDefined, ErrorConnectB2P
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE_WITH_ID = 'MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='
SIGNATURE_WITH_REFERENCE = 'MWNkMzBjZGEwYjhlMjhjY2MyY2Q1NjJiYzlmN2JjNTU='


def test_default_init_complete(order):
    assert order.sector == '0000'
    assert order.id == '1'
    assert order.b2p_token == 'test'
    assert order.mode == 0
    assert order.get_token == 0
    assert not order.is_prod_stand


def test_init_complete():
    order = Order('0000', 'test', '1', 'reference', mode=1, get_token=1, is_prod_stand=True)

    assert order.sector == '0000'
    assert order.id == '1'
    assert order.reference == 'reference'
    assert order.b2p_token == 'test'
    assert order.get_token == 1
    assert order.mode == 1
    assert order.is_prod_stand


def test_init_error():
    with pytest.raises(ErrorDefined, match=''):
        Order('0000', 'test')


def test_init_not_allowed_params():
    operation = Order('0000', '1', 1000, 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Order' object has no attribute 'not_valid_field'"):
        operation.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(operation):
    assert operation._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_date_by_send_b2p(order):
    assert order._get_date_by_send_b2p() == {'get_token': 0,  # pylint: disable=protected-access
                                             'id': '1',
                                             'mode': 0,
                                             'sector': '0000',
                                             'signature': SIGNATURE_WITH_ID,
                                             }

    assert 'is_prod_stand' in order.__dict__
    assert 'b2p_token' in order.__dict__


def test_get_url_b2p_prod_stand():
    complete = Order('0000', 'test', 1000, 'test', is_prod_stand=True)

    assert complete._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    complete = Order('0000', 'test', 1000,
                     'test', stand_url='https://custom.best2pay.net')

    assert complete._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature_wirth_id(order):
    assert order._create_signature() == SIGNATURE_WITH_ID  # pylint: disable=protected-access


def test_create_signature_wirth_reference():
    order = Order('0000', 'test', 'reference')
    assert order._create_signature() == SIGNATURE_WITH_REFERENCE  # pylint: disable=protected-access


def test_get_date_by_send_b2p_check(order):
    """Проверяем отсутствие значений по умолчанию"""
    data = order._get_date_by_send_b2p()  # pylint: disable=protected-access
    assert 'reference' not in data
    order = Order('0000', 'test', reference='reference')
    data = order._get_date_by_send_b2p()  # pylint: disable=protected-access
    assert 'id' not in data


def test_do(mocker, response_xml_success_b2p, order):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())
    order = order.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Order',)
    assert kwargs == {
        'data': {
            'id': '1',
            'sector': '0000',
            'mode': 0,
            'get_token': 0,
            'signature': SIGNATURE_WITH_ID,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_error(mocker, response_xml_error_b2p, order):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = order.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Order',)
    assert kwargs == {
        'data': {
            'id': '1',
            'sector': '0000',
            'get_token': 0,
            'mode': 0,
            'signature': SIGNATURE_WITH_ID,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, order):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        order.do()

    assert mock.call_count == 3
