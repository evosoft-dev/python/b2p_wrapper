import pytest

from src.wrapper_b2p import Authorize
from src.wrapper_b2p.exception import ErrorPayerIDNotExist
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE_WITHOUT_SAVE_CARD = 'MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='
SIGNATURE_WITH_SAVE_CARD = 'M2ZmNzk0ZWY2N2Y5ZjJiYjRjZjQ5ZTJjNTM4NmQyODI='


def test_default_init_authorize():
    authorize = Authorize('0000', '1', 'test')

    assert authorize.sector == '0000'
    assert authorize.id == '1'
    assert authorize.b2p_token == 'test'
    assert authorize.get_token == 0
    assert not authorize.token
    assert not authorize.payer_id
    assert not authorize.save_card
    assert not authorize.is_prod_stand


def test_init_authorize():
    authorize = Authorize('0000', '1', 'test', **{
        'get_token': 1,
        'token': 'token',
        'payer_id': '1',
        'save_card': 'true',
        'is_prod_stand': True
    })
    assert authorize.sector == '0000'
    assert authorize.id == '1'
    assert authorize.b2p_token == 'test'
    assert authorize.get_token == 1
    assert authorize.token == 'token'
    assert authorize.payer_id == '1'
    assert authorize.save_card == 'true'
    assert authorize.is_prod_stand


def test_init_not_allowed_params():
    authorize = Authorize('0000', '1', 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Authorize' object has no attribute 'not_valid_field'"):
        authorize.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(authorize):
    assert authorize._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    authorize = Authorize('0000', '1', 'test', is_prod_stand=True)

    assert authorize._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    authorize = Authorize('0000', '1', 'test', stand_url='https://custom.best2pay.net')

    assert authorize._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature_without_save_card(authorize):
    assert authorize._create_signature() == SIGNATURE_WITHOUT_SAVE_CARD  # pylint: disable=protected-access


def test_create_signature_with_save_card():
    authorize = Authorize('0000', '1', 'test', save_card='true', payer_id=1)

    assert authorize._create_signature() == SIGNATURE_WITH_SAVE_CARD  # pylint: disable=protected-access


def test_create_signature_raise_exception():
    with pytest.raises(ErrorPayerIDNotExist, match=''):
        Authorize('0000', '1', 'test', save_card='true')


@pytest.mark.parametrize(
    'authorize_instance, get_params',
    [
        (Authorize('0000', '1', 'test'), 'sector=0000&id=1&'
                                         'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='),
        (Authorize('0000', '1', 'test', get_token=1), 'sector=0000&id=1&get_token=1&'
                                                      'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='),
        (Authorize('0000', '1', 'test', get_token=1, payer_id=1), 'sector=0000&id=1&get_token=1&p'
                                                                  'ayer_id=1&signature=MWU0N2MyODBiOTUyZWYyYzljZ'
                                                                  'TI4N2JkODUzYWRlNGI='),
    ]
)
def test_get_params(authorize_instance, get_params):
    assert authorize_instance._get_params() == get_params  # pylint: disable=protected-access


def test_do(authorize):
    assert authorize.do() == 'https://test.best2pay.net/webapi/Authorize?sector=0000&id=1&' \
                             'signature=MWU0N2MyODBiOTUyZWYyYzljZTI4N2JkODUzYWRlNGI='
