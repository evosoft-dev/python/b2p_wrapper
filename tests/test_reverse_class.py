from collections import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import Reverse
from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p.settings import URL_PROD_STAND, URL_TEST_STAND

SIGNATURE = 'ZGNiOGY3YzZhYmM2ZjE5NDhjN2U1NjdjY2NlZmE1ZGM='


def test_default_init_complete(reverse):
    assert reverse.sector == '0000'
    assert reverse.id == '1'
    assert reverse.amount == 1000
    assert reverse.b2p_token == 'test'
    assert not reverse.is_prod_stand


def test_init_complete():
    reverse = Reverse('0000', '1', 1000, 'test', fee=300, is_prod_stand=True)

    assert reverse.sector == '0000'
    assert reverse.id == '1'
    assert reverse.amount == 1000
    assert reverse.b2p_token == 'test'
    assert reverse.is_prod_stand


def test_init_not_allowed_params():
    reverse = Reverse('0000', '1', 1000, 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Reverse' object has no attribute 'not_valid_field'"):
        reverse.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(reverse):
    assert reverse._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    reverse = Reverse('0000', '1', 1000, 'test', is_prod_stand=True)

    assert reverse._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    reverse = Reverse('0000', '1', 1000,
                      'test', stand_url='https://custom.best2pay.net')

    assert reverse._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_get_date_by_send_b2p(reverse):
    assert reverse._get_date_by_send_b2p() == {'amount': 1000,  # pylint: disable=protected-access
                                               'sector': '0000',
                                               'currency': 643,
                                               'id': '1',
                                               'signature': SIGNATURE,
                                               }

    assert 'is_prod_stand' in reverse.__dict__
    assert 'b2p_token' in reverse.__dict__


def test_create_signature(reverse):
    assert reverse._create_signature() == SIGNATURE  # pylint: disable=protected-access


def test_do(mocker, response_xml_success_b2p, reverse):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())
    order = reverse.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Reverse',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_error(mocker, response_xml_error_b2p, reverse):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = reverse.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Reverse',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, reverse):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        reverse.do()

    assert mock.call_count == 3
