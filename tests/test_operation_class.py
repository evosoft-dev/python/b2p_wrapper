from collections import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import Operation
from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE = 'M2ZmNzk0ZWY2N2Y5ZjJiYjRjZjQ5ZTJjNTM4NmQyODI='


def test_default_init_complete(operation):
    assert operation.sector == '0000'
    assert operation.id == '1'
    assert operation.operation == '1'
    assert operation.b2p_token == 'test'
    assert operation.get_token == 0
    assert not operation.is_prod_stand


def test_init_complete():
    operation = Operation('0000', '1', '1', 'test', get_token=1, is_prod_stand=True)

    assert operation.sector == '0000'
    assert operation.id == '1'
    assert operation.operation == '1'
    assert operation.b2p_token == 'test'
    assert operation.get_token == 1
    assert operation.is_prod_stand


def test_init_not_allowed_params():
    operation = Operation('0000', '1', 1000, 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Operation' object has no attribute 'not_valid_field'"):
        operation.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(operation):
    assert operation._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    complete = Operation('0000', '1', 1000, 'test', is_prod_stand=True)

    assert complete._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    complete = Operation('0000', '1', 1000,
                         'test', stand_url='https://custom.best2pay.net')

    assert complete._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_get_date_by_send_b2p(operation):
    assert operation._get_date_by_send_b2p() == {'get_token': 0,  # pylint: disable=protected-access
                                                 'id': '1',
                                                 'operation': '1',
                                                 'sector': '0000',
                                                 'signature': SIGNATURE,
                                                 }

    assert 'is_prod_stand' in operation.__dict__
    assert 'b2p_token' in operation.__dict__


def test_create_signature(operation):
    assert operation._create_signature() == SIGNATURE  # pylint: disable=protected-access


def test_do(mocker, response_xml_success_b2p, operation):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())
    order = operation.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Operation',)
    assert kwargs == {
        'data': {
            'id': '1',
            'get_token': 0,
            'sector': '0000',
            'operation': '1',
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_error(mocker, response_xml_error_b2p, operation):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = operation.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Operation',)
    assert kwargs == {
        'data': {
            'id': '1',
            'operation': '1',
            'sector': '0000',
            'get_token': 0,
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, operation):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        operation.do()

    assert mock.call_count == 3
