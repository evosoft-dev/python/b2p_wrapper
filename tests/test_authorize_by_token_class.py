from collections import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import AuthorizeByToken
from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE = 'NWZmNjFhZjdhOTZhOWZhNDU0Yzc4YThhYjQ2OTgyNjk='


def test_default_init_purchase():
    authorize_by_token = AuthorizeByToken('0000', 1, 'token', 'test')

    assert authorize_by_token.sector == '0000'
    assert authorize_by_token.id == 1
    assert authorize_by_token.token == 'token'
    assert authorize_by_token.b2p_token == 'test'
    assert authorize_by_token.cvc == ''
    assert not authorize_by_token.is_prod_stand


def test_init_purchase():
    authorize_by_token = AuthorizeByToken('0000', 1, 'token', 'test', **{'cvc': 'cvc', 'is_prod_stand': True})

    assert authorize_by_token.sector == '0000'
    assert authorize_by_token.id == 1
    assert authorize_by_token.token == 'token'
    assert authorize_by_token.b2p_token == 'test'
    assert authorize_by_token.cvc == 'cvc'
    assert authorize_by_token.is_prod_stand


def test_init_not_allowed_params():
    authorize_by_token = AuthorizeByToken('0000', '1', 'token', 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'AuthorizeByToken' object has no attribute 'not_valid_field'"):
        authorize_by_token.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(authorize_by_token):
    assert authorize_by_token._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    authorize_by_token = AuthorizeByToken('0000', '1', 'token', 'test', is_prod_stand=True)

    assert authorize_by_token._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    authorize_by_token = AuthorizeByToken('0000', '1', 'token',
                                          'test', stand_url='https://custom.best2pay.net')

    assert authorize_by_token._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature(authorize_by_token):
    assert authorize_by_token._create_signature() == SIGNATURE  # pylint: disable=protected-access


def test_do_response_success_to_b2p(mocker, response_xml_success_b2p, authorize_by_token):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())

    order = authorize_by_token.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/AuthorizeByToken',)
    assert kwargs == {
        'data': {
            'cvc': '',
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
            'token': 'token',
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_error_to_b2p(mocker, response_xml_error_b2p, authorize_by_token):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = authorize_by_token.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/AuthorizeByToken',)
    assert kwargs == {
        'data': {
            'cvc': '',
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
            'token': 'token',
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, authorize_by_token):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        authorize_by_token.do()

    assert mock.call_count == 3
