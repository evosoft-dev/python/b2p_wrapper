import pytest

from src.wrapper_b2p import AuthorizeInc
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE_WITHOUT_FEE = 'ZGNiOGY3YzZhYmM2ZjE5NDhjN2U1NjdjY2NlZmE1ZGM='
SIGNATURE_WITH_FEE = 'YThmY2YzYTllMTQzMTQ3NjlhZDlkOTYyNDBhZTkyMGU='


def test_default_init_authorize_inc(authorize_inc):
    assert authorize_inc.sector == '0000'
    assert authorize_inc.id == '1'
    assert authorize_inc.amount == 1000
    assert authorize_inc.b2p_token == 'test'
    assert authorize_inc.fee == 0
    assert not authorize_inc.is_prod_stand


def test_init_authorize_inc():
    authorize_inc = AuthorizeInc('0000', '1', 1000, 'test', fee=300, is_prod_stand=True)

    assert authorize_inc.sector == '0000'
    assert authorize_inc.id == '1'
    assert authorize_inc.amount == 1000
    assert authorize_inc.b2p_token == 'test'
    assert authorize_inc.fee == 300
    assert authorize_inc.is_prod_stand


def test_init_not_allowed_params():
    authorize_inc = AuthorizeInc('0000', '1', 1000, 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'AuthorizeInc' object has no attribute 'not_valid_field'"):
        authorize_inc.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(authorize_inc):
    assert authorize_inc._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    authorize_inc = AuthorizeInc('0000', '1', 1000, 'test', is_prod_stand=True)

    assert authorize_inc._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    authorize = AuthorizeInc('0000', '1', 1000,
                             'test', stand_url='https://custom.best2pay.net')

    assert authorize._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature_without_fee(authorize_inc):
    assert authorize_inc._create_signature() == SIGNATURE_WITHOUT_FEE  # pylint: disable=protected-access


def test_create_signature_with_fee():
    purchase = AuthorizeInc('0000', '1', 1000, 'test', fee=300)
    assert purchase._create_signature() == SIGNATURE_WITH_FEE  # pylint: disable=protected-access


@pytest.mark.skip
def test_do(authorize_inc):
    # Сейчас этот функционал не работает на стороне b2p
    assert authorize_inc.do() == {}


@pytest.mark.skip
def test_do_error(authorize_inc):
    # Сейчас этот функционал не работает на стороне b2p
    assert authorize_inc.do() == {}
