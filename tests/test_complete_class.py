from collections import OrderedDict

import pytest
from requests import RequestException

from src.wrapper_b2p import Complete
from src.wrapper_b2p.exception import ErrorConnectB2P
from src.wrapper_b2p.settings import URL_TEST_STAND, URL_PROD_STAND

SIGNATURE = 'ZGNiOGY3YzZhYmM2ZjE5NDhjN2U1NjdjY2NlZmE1ZGM='


def test_default_init_complete(complete):
    assert complete.sector == '0000'
    assert complete.id == '1'
    assert complete.amount == 1000
    assert complete.b2p_token == 'test'
    assert complete.fee == 0
    assert not complete.is_prod_stand


def test_init_complete():
    complete = Complete('0000', '1', 1000, 'test', fee=300, is_prod_stand=True)

    assert complete.sector == '0000'
    assert complete.id == '1'
    assert complete.amount == 1000
    assert complete.b2p_token == 'test'
    assert complete.fee == 300
    assert complete.is_prod_stand


def test_init_not_allowed_params():
    complete = Complete('0000', '1', 1000, 'test', **{
        'not_valid_field': 1,
    })

    with pytest.raises(AttributeError, match="'Complete' object has no attribute 'not_valid_field'"):
        complete.not_valid_field  # pylint: disable=pointless-statement


def test_get_url_b2p_test_stand(complete):
    assert complete._get_url_b2p() == URL_TEST_STAND  # pylint: disable=protected-access


def test_get_url_b2p_prod_stand():
    complete = Complete('0000', '1', 1000, 'test', is_prod_stand=True)

    assert complete._get_url_b2p() == URL_PROD_STAND  # pylint: disable=protected-access


def test_get_url_b2p_custom_stand():
    complete = Complete('0000', '1', 1000,
                        'test', stand_url='https://custom.best2pay.net')

    assert complete._get_url_b2p() == 'https://custom.best2pay.net'  # pylint: disable=protected-access


def test_create_signature(complete):
    assert complete._create_signature() == SIGNATURE  # pylint: disable=protected-access


def test_do(mocker, response_xml_success_b2p, complete):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_success_b2p})())
    order = complete.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Complete',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'fee': 0,
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_error(mocker, response_xml_error_b2p, complete):
    mock = mocker.patch('requests.post', return_value=type('Response', (), {'text': response_xml_error_b2p})())

    order = complete.do()

    assert mock.call_count == 1
    args, kwargs = mock.call_args
    assert args == ('https://test.best2pay.net/webapi/Complete',)
    assert kwargs == {
        'data': {
            'amount': 1000,
            'currency': 643,
            'fee': 0,
            'id': '1',
            'sector': '0000',
            'signature': SIGNATURE,
        }
    }
    assert isinstance(order, OrderedDict)


def test_do_response_raise_error_to_b2p(mocker, complete):
    mock = mocker.patch('requests.post', side_effect=RequestException())

    with pytest.raises(ErrorConnectB2P, match=''):
        complete.do()

    assert mock.call_count == 3
