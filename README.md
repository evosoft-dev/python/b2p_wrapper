# Обертка над документацией b2p

Библиотека оборачивает следующие методы:

- webapi/Register (регистрация заказа)
        обязательные параметры:
            - sector
            - amount
            - currency
            - description
            - signature
        не обязательные параметры:
            - fee
            - url
            - failurl
            - lang
            - is_prod_stand (отвечает за выбор сервер оплаты тест/прод)

- webapi/Purchase (оплата по карте)
        обязательные параметры:
            - sector
            - id
            - signature
        не обязательные параметры:
            - get_token
            - token
            - payer_id
            - save_card
            - is_prod_stand (отвечает за выбор сервер оплаты тест/прод)

- webapi/Authorize (холдирование средств на карте)
        обязательные параметры:
            - sector
            - id
            - signature
        не обязательные параметры:
            - get_token
            - token
            - payer_id
            - save_card
            - is_prod_stand (отвечает за выбор сервер оплаты тест/прод)

- webapi/AuthorizeInc (увеличение суммы холдирования) (не прошел ручное тестирование, не реализованно сейчас на сервере b2p)
        обязательные параметры:
            - sector
            - id
            - amount
            - currency
            - signature
        не обязательные параметры:
            - fee
            - is_prod_stand (отвечает за выбор сервер оплаты тест/прод)

- webapi/Complete (завершение оплаты по карте используется только для за холдированных средств)
        обязательные параметры:
            - sector
            - id
            - amount
            - currency
            - signature
        не обязательные параметры:
            - fee
            - is_prod_stand (отвечает за выбор сервер оплаты тест/прод)

- webapi/Reverse (отмена операции)
        обязательные параметры:
            - sector
            - id
            - amount
            - currency
            - signature

- webapi/Operation (получение информации об операции)
         обязательные параметры:
            - sector
            - id
            - operation
            - signature
        не обязательные параметры:
            - get_token

- webapi/Order (получение информации по заказу)
        обязательные параметры:
            - sector
            - signature
        условно-обязательные параметры (хотя бы один из них должен быть в запросе)
            - id
            - reference
        не обязательные параметры:
            - mode
            - get_token
  
- webapi/PurchaseByToken (оплата по токену карты)
        обязательные параметры:
            - sector
            - id
            - token
            - signature
        не обязательные параметры:
            - cvc
  
- webapi/AuthorizeByToken (холдирование по токену карты)
         обязательные параметры:
            - sector
            - id
            - token
            - signature
        не обязательные параметры:
            - cvc
  
Не обязательные парамеры передаются в kwargs, allowed_field_with_default_value_to_b2p тут указаны допустимые параметры, 
которые буду обрабатываться 

## Тестовые номера карт:

2200200111114591, UNKNOWN NAME, 05/2022, 426 (с ошибкой)
5570725111081379, UNKNOWN NAME, 05/2022, 415 с 3ds
4809388889655340, UNKNOWN NAME, 05/2022, 195
